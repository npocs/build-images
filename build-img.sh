#!/bin/sh

image=$1
startdir=$2
registry=$3
project=$4

master_build=0
if test "$CI_PROJECT_NAMESPACE" = "libssh" && test "$CI_PROJECT_NAME" = "build-images";then
	master_build=1
fi

set -e
docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $registry
docker build --no-cache -t $registry/$project:$image $startdir

# Try to build libssh in the new image
SH="/bin/bash"
CMAKE="cmake"
CMAKE_DEFAULT_OPTIONS="-DCMAKE_BUILD_TYPE=RelWithDebInfo -DPICKY_DEVELOPER=ON"
CMAKE_BUILD_OPTIONS="-DWITH_BLOWFISH_CIPHER=ON -DWITH_SFTP=ON -DWITH_SERVER=ON -DWITH_ZLIB=ON -DWITH_PCAP=ON -DWITH_DEBUG_CRYPTO=ON -DWITH_DEBUG_PACKET=ON -DWITH_DEBUG_CALLTRACE=ON -DWITH_DSA=ON"
CMAKE_TEST_OPTIONS="-DUNIT_TESTING=ON -DCLIENT_TESTING=ON -DSERVER_TESTING=ON -DWITH_BENCHMARKS=ON"
CMAKE_OPTIONS="$CMAKE_DEFAULT_OPTIONS $CMAKE_BUILD_OPTIONS $CMAKE_TEST_OPTIONS"
GIT_REPO="https://gitlab.com/libssh/libssh-mirror.git"
GIT_BRANCH="master"
if [ "$image" == "buildenv-centos7" ]; then
	CMAKE="cmake3"
	GIT_BRANCH="stable-0.10"
elif [ "$image" == "buildenv-alpine" ]; then
	SH="/bin/sh"
	CMAKE_OPTIONS="$CMAKE_DEFAULT_OPTIONS -DWITH_SFTP=ON -DWITH_SERVER=ON -DWITH_ZLIB=ON -DWITH_PCAP=ON -DUNIT_TESTING=ON"
elif [ "$image" == "buildenv-mingw" ]; then
	CMAKE_OPTIONS="$CMAKE_DEFAULT_OPTIONS -DWITH_SFTP=ON -DWITH_SERVER=ON -DWITH_ZLIB=ON -DWITH_PCAP=ON -DUNIT_TESTING=ON"
	CMAKE="mingw64-cmake"
elif [ "$image" == "buildenv-fedora" ]; then
	CMAKE_ADDITIONAL_OPTIONS="-DWITH_PKCS11_URI=ON"
fi
docker run -i $registry/$project:$image $SH <<EOL
export WINEPATH=/usr/x86_64-w64-mingw32/sys-root/mingw/bin
export WINEDEBUG=-all
export OPENSSL_ENABLE_SHA1_SIGNATURES=1
uname -a && \
cat /etc/os-release && \
mount && \
df -h && \
cat /proc/swaps && \
free -h && \
git clone ${GIT_REPO} && \
cd libssh-mirror && \
git checkout ${GIT_BRANCH} && \
mkdir -p obj && cd obj && \
$CMAKE $CMAKE_OPTIONS $CMAKE_ADDITIONAL_OPTIONS .. && \
make -j\$(nproc) && \
make -j\$(nproc) install && \
ctest --output-on-failure
EOL

# Continue only if it worked
if [ "$?" != 0 ]; then
	echo "Test build in the new image failed"
	exit 1
fi

if test $master_build = 0; then
	echo "Not a master build"
	exit 0
else
	docker push $registry/$project:$image
fi

exit 0
